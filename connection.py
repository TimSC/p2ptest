import socket
import time
import random
import msgpack

# **************************************************************************************

class SocketUdp(object):
	def __init__(self, node):
		self.node = node
		self.sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
		self.sock.bind(('', 0))
		self.ip = self.sock.getsockname()[0]
		self.port = self.sock.getsockname()[1]
		self.retTime = None

	def SendMessage(self, toAddr, toConnection, fromAddr, fromConnection, msg, delay=0.0):
		#print ("Send Message", toAddr, toConnection, fromAddr, fromConnection)
		splitToConnection = toConnection.split(":")
		self.sock.sendto(msg, (splitToConnection[0], int(splitToConnection[1])))
		
	def ReceiveMessage(self):

		startTime = time.time()
		self.sock.settimeout(0.05)
		try:
			msg, (fromHost, fromPort) = self.sock.recvfrom(1024*100)
		except (socket.timeout, BlockingIOError):
			self.retTime = time.time()
			return
	
		receivedMessages = [(msg, fromHost, fromPort)]

		self.sock.settimeout(0.0)
		while True:
			try:
				msg, (fromHost, fromPort) = self.sock.recvfrom(1024*100)
			except (socket.timeout, BlockingIOError):
				for msg, fromHost, fromPort in receivedMessages:
					self.node.ReceiveMessage("{}:{}".format(fromHost, fromPort), msg)	
				self.retTime = time.time()
				return

			receivedMessages.append((msg, fromHost, fromPort))

	def GetConnection(self):
		return "{}:{}".format(self.ip, self.port)

def RequestSocketUdp(node):

	sock = SocketUdp(node)

	#Don't add the new address to routing table as this breaks bootstrapping the new
	node.connection = sock.GetConnection()
	node.sock = sock


