#!/usr/bin/python3
# -*- coding: utf-8 -*-
#Based on "Kademlia: A Peer-to-peer Information System Based on the XOR Metric"
#Lecture Notes in Computer Science 2429
#Petar Maymounkov and David Mazières

import random
import math
import time
import msgpack
import itertools
import connection
import multiprocessing
import multiprocessing.connection as mpc
import struct
from operator import xor
addrBits = 16
maxAddr = pow(2,addrBits)

def Distance(a, b):
	if a is None or b is None:
		return maxAddr-1 #Assume the worst
	return a^b

class RoutingItem(object):
	def __init__(self, addr, connection):
		self.addr = addr
		self.connection = connection
		self.failures = 0
		self.lastSeen = time.time()
		self.activePing = None
		self.findNode = {}
		self.removePending = False
		self.pingDuration = []
		self.findNodeDuration = []

	def __repr__(self):
		return str((self.addr, self.connection, self.failures))

	def AddPingDuration(self, t):
		self.pingDuration.append(t)
		self.pingDuration = self.pingDuration[-10:]

	def AddFindNodeDuration(self, t):
		self.findNodeDuration.append(t)
		self.findNodeDuration = self.findNodeDuration[-10:]

	def HasNonce(self, nonce): 
		if self.activePing is not None:
			if self.activePing[1] == nonce:
				return True
		for addr, q in self.findNode.items():
			if q[2] == nonce:
				return True
		return False

	def HandlePingResponse(self, msgNonce):
		if self.activePing is None: 
			print ("received unexpected ping response")
			return False
		sendTime = self.activePing[0]
		nonceExpected = self.activePing[1]
		if msgNonce == nonceExpected:
			self.AddPingDuration(time.time() - sendTime)
			self.activePing = None
			return True
		return False

	def HandleFindNodeSuccess(self, msgNonce, fromAddr, fromConnection, result):
		
		if fromAddr in self.findNode:
			query = self.findNode[fromAddr]
			queryKey = fromAddr
		elif None in self.findNode and self.findNode[None][2] == msgNonce:
			query = self.findNode[None]
			queryKey = None
		else:
			return

		sendTime = query[0]
		listeners = query[1]
		nonceExpected = query[2]
		if msgNonce == nonceExpected:
			self.AddFindNodeDuration(time.time() - sendTime)
			del self.findNode[queryKey]
			for li in listeners:
				li.AsyncFindNodeSuccess(fromAddr, fromConnection, result)

class FindClosest(object):
	def __init__(self, node, addr, isBoot):

		self.node = node
		self.targetAddr = addr
		self.numConcurrentSearches = 3
		self.ignoreList = set()
		self.bestFound = None
		self.bestConnection = None
		self.bestDist = None
		self.queryDgnstc = []
		self.queryDgnstc.append(("FindClosest", node.address, "to find", self.targetAddr))
		self.statusFinished = False
		self.statusSearchingRemote = False
		self.statusResultsPending = False
		self.querySuccesses = []
		self.failedOnAddrs = set()
		self.stepCount = 0
		self.listeners = []
		self.isBoot = isBoot

	def Initialize(self):

		self.queryDgnstc.append(("starting iteration", self.node.address, "to find", self.targetAddr))
		#print (self.node.address, "FindClosest it", self.bestFound)
		self.cursor = self.node.FindClosestInRouting(self.targetAddr, self.numConcurrentSearches, self.ignoreList)

		#print ("cursor", self.cursor)
		self.ignoreList.update([c[0] for c in self.cursor])
		if len(self.cursor) == 0:
			self.queryDgnstc.append(("no more peers, return best found", self.bestFound))
			self.statusFinished = True
			self.SendResultToListeners()
			return

		#Evaluate our starting point nodes
		self.prevDistAddr = [(Distance(ckb[0], self.targetAddr), ckb) for ckb in self.cursor]
		self.prevDistAddr.sort(key=lambda a: a[0])
		self.queryDgnstc.append(("initial peers", self.prevDistAddr))

		if self.prevDistAddr[0][1][0] == self.targetAddr: #Found the target, so stop
			self.queryDgnstc.append(("found the target node in routing table", self.targetAddr, self.prevDistAddr[0][1][0]))
			self.bestDist = self.prevDistAddr[0][0]
			self.bestFound = self.prevDistAddr[0][1][0]
			self.bestConnection = self.prevDistAddr[0][1][1]
			self.statusFinished = True
			self.SendResultToListeners()
			return

		if self.bestDist is None or self.prevDistAddr[0][0] < self.bestDist:
			self.bestDist = self.prevDistAddr[0][0] #Retain the best result found so far
			self.bestFound = self.prevDistAddr[0][1][0]
			self.bestConnection = self.prevDistAddr[0][1][1]

	def RequestRemoteFindNode(self):

		if self.statusFinished: raise RuntimeError("Processing already finished")

		self.queryDgnstc.append(("step", self.stepCount, self.cursor))
		self.ignoreList.update([c[0] for c in self.cursor])

		self.foundList = []
		for ckb, conn in self.cursor:
			routingItem, bucket, bucketNum = self.node.FindAddrInRoutingTable(ckb, self.isBoot)
			
			if ckb not in routingItem.findNode:
				#Create new query
				nonce = random.randint(0, pow(2, 64))
				routingItem.findNode[ckb] = [time.time(), [self], nonce]
				ret = self.node.sock.SendMessage(
					routingItem.addr, routingItem.connection, self.node.address, self.node.connection, 
						msgpack.packb({'type': 'find_node', 'address': self.targetAddr, 'nonce': nonce, 'sender':self.node.address}))

			else:
				#Piggy back on existing query
				existingQuery = routingItem.findNode[ckb]
				existingQuery[1].append(self)

	def AsyncFindNodeFailed(self, ckb):
		#print ("AsyncFindNodeFailed")
		self.foundList.append(None)
		if ckb is not None:
			self.node.DemoteAddrInRoutingTable(ckb)
		self.failedOnAddrs.add(ckb)

	def AsyncFindNodeSuccess(self, fromAddr, fromConnection, found):
		#print ("AsyncFindNodeSuccess")
		self.queryDgnstc.append(("received response from", fromAddr, found))
		self.foundList.append(found)
		self.querySuccesses.append((fromAddr, fromConnection, found))

	def ProcessRemoteResults(self):

		if self.statusFinished: raise RuntimeError("Processing already finished")

		foundListCombined = []
		for fl in self.foundList:
			if fl is None: continue
			foundListCombined.extend(fl)
		foundListCombined = [tuple(a) for a in foundListCombined]
		foundListCombined = set(foundListCombined) #Remove duplicates and own address
		foundListCombined = [(addr, connection) for addr, connection in foundListCombined if addr != self.node.address]

		for addr, connection in foundListCombined:
			if addr != self.targetAddr: continue
			self.queryDgnstc.append(("stopping queries as target found"))
			self.bestDist = 0
			self.bestFound = self.targetAddr
			self.bestConnection = connection
			self.statusFinished = True
			self.SendResultToListeners()
			return

		for addrFound, connectionFound in foundListCombined:
			assert addrFound != self.node.address
			self.node.AddAddrToRoutingTable(addrFound, connectionFound, True) #Remember this as it will come in useful

		if len(foundListCombined) == 0:
			self.queryDgnstc.append(("not found anything, try a different set of starting nodes"))
			self.statusSearchingRemote = False
			return #Give up and try a different set of starting nodes

		distAddr = [(Distance(ckb[0], self.targetAddr), ckb) for ckb in foundListCombined]
		distAddr.sort(key=lambda a: a[0])
		if distAddr[0][1] == self.targetAddr: #Found the target, so stop
			self.queryDgnstc.append(("found the target node"))
			self.bestDist = distAddr[0][0]
			self.bestFound = distAddr[0][1][0]
			self.bestConnection = distAddr[0][1][1]
			self.statusSearchingRemote = False
			self.statusFinished = True
			self.SendResultToListeners()
			return

		if self.bestDist is None or distAddr[0][0] < self.bestDist:
			self.bestDist = distAddr[0][0] #Retain the best result found so far
			self.bestFound = distAddr[0][1][0]
			self.bestConnection = distAddr[0][1][1]

		#Find the closest addresses and pick best numConcurrentSearches (usually 3)
		closerAddrs = []
		for dist, addr2 in distAddr:
			if addr2 in self.ignoreList: continue
			if dist < self.prevDistAddr[0][0]:
				closerAddrs.append(addr2)

		if len(closerAddrs) == 0:
			self.statusSearchingRemote = False
			return #Reached a dead end, restart with different nodes

		self.cursor = closerAddrs[:self.numConcurrentSearches]
		self.stepCount += 1
		self.prevDistAddr = distAddr

	def Update(self):
		if self.statusFinished: return
		
		if not self.statusSearchingRemote:

			if self.stepCount <= addrBits:
				#Start query with a set of starting nodes
				self.Initialize()
				self.stepCount = 0
				self.statusSearchingRemote = True

				if not self.statusFinished:
					self.RequestRemoteFindNode()

					self.statusResultsPending = True

			else:
				#Give up
				self.statusFinished = True
				self.SendResultToListeners()

		else:
			if not self.statusResultsPending:
				if not self.statusFinished and self.statusSearchingRemote:
					self.ProcessRemoteResults()

				if not self.statusFinished and self.statusSearchingRemote:
					self.RequestRemoteFindNode()

					self.statusResultsPending = True

			else:
				if len(self.foundList) >= len(self.cursor):
					self.statusResultsPending = False

	def SendResultToListeners(self):
		for li in self.listeners:
			li.FindNodeFinished(self.targetAddr, self.bestFound, self.bestConnection, self.queryDgnstc)

		#print ([(li[0], li[1]) for li in self.querySuccesses])

		if len(self.failedOnAddrs) > 0:
			#Inspired by Improving the Performance and Robustness of Kademlia-Based Overlay Networks
			#Andreas Binzenhöfer, Holger Schnabel
			#Inform other nodes about connecting difficulties encountered during the search

			for addr, conn, found in self.querySuccesses:
				matches = []
				for foundAddr, foundConn in found:
					if foundAddr in self.failedOnAddrs:
						matches.append(foundAddr)
				if len(matches) > 0:
					self.node.SendDownlistNotify(addr, matches)

class Bootstrap(object):
	def __init__(self, node, joinConnection, RequestSocket):	
		self.joinAddr = None
		self.joinConnection = joinConnection
		self.RequestSocket = RequestSocket
		self.running = True
		self.stepNum = 1
		self.node = node
		self.findNodePending = 0
		self.findNodeReceived = 0

	def BootstrapStep1(self):
		self.stepNum = 2

		if self.joinConnection is not None:
			
			self.RequestSocket(self.node)
			print ("Establishing peer", self.node.address, self.node.connection)
			if self.node.connection == self.joinConnection:
				raise RuntimeError("Cannot acquire connect if we plan to use that connection")
			self.node.AddAddrToRoutingTable(None, self.joinConnection, False, True)

		else:
			#This is the boot node
			self.RequestSocket(self.node)
			print ("Establishing peer", self.node.address, self.node.connection)

		if self.node.pipe is not None:
			self.node.pipe.send(('address', self.node.address, self.node.connection))

		self.stepNum = 3

	def BootstrapStep3(self):

		self.stepNum = 4

		if self.joinConnection is not None:
			#Look up own address and get join node's address
			self.node.FindClosestAsync(self.node.address, [self], True)

		else:
			self.stepNum = 5

	def BootstrapStep5(self):
		#Complete routing table
		self.stepNum = 6

		if self.joinConnection is not None:

			#Join node is not necessarily in our routing table, so we ping it now TODO
			try:
				self.node.PingAddr(self.joinAddr, True)
			except RuntimeError:
				print ("Failed to ping join addr, not in routing table?")

			self.node.RemoveAddrFromRoutingTable(None, True) #Remove boot node

			startDist = int(math.log(Distance(self.joinAddr, self.node.address), 2))
			self.findNodePending = 0
			self.findNodeReceived = 0

			self.findNodePending = len(range(startDist, addrBits))

			for bucketNum in range(startDist, addrBits):
				#Generate address in this basket
				addr = self.node.GenerateAddrInBucket(bucketNum)

				self.node.FindClosestAsync(addr, [self])

		else:
			self.stepNum = 7
				
	def BootstrapStep7(self):
		self.stepNum = None
		self.running = False
		self.node.pipe.send(('ready', True))

	def Update(self):
		if not self.running: return

		if self.stepNum == 1:
			self.BootstrapStep1()

		elif self.stepNum == 3:
			self.BootstrapStep3()

		elif self.stepNum == 5:
			self.BootstrapStep5()

		elif self.stepNum == 7:
			self.BootstrapStep7()

	def FindNodeFinished(self, targetAddr, foundAddr, foundConnection, diagnostics):
		#print ("FindNodeFinished", targetAddr, foundAddr)

		if self.stepNum == 4 and targetAddr == self.node.address:
			self.stepNum = 5

		if self.stepNum == 6:
			self.findNodeReceived += 1
			if foundAddr is not None:
				self.node.AddAddrToRoutingTable(foundAddr, foundConnection, True)
			if self.findNodeReceived >= self.findNodePending:
				self.stepNum = 7

class RefreshBucket(object):
	def __init__(self, node, bucketNum):
		self.node = node
		self.bucketNum = bucketNum
		self.pending = False
		self.randomAddr = None
		self.deletePending = False

	def Refresh(self):
		self.randomAddr = self.node.GenerateAddrInBucket(self.bucketNum)
		print (self.node.address, "not accessed bucket", self.bucketNum, "in a while, updating", self.randomAddr)
		self.pending = True
		self.node.FindClosestAsync(self.randomAddr)

	def FindNodeFinished(self, targetAddr, foundAddr, foundConnection, diagnostics):
		if foundAddr == self.randomAddr:
			#Amazingly, we found a valid address
			self.node.AddAddrToRoutingTable(foundAddr, foundConnection, True)		
		self.pending = False
		self.deletePending = True

class FindNodeResultToPipe(object):
	def __init__(self, pipe):
		self.pipe = pipe

	def FindNodeFinished(self, targetAddr, bestAddr, bestConnection, queryDgnstc):
		self.pipe.send(("find_node_response", targetAddr, bestAddr, bestConnection, queryDgnstc))

class Node(object):
	def __init__(self, pipe, newAddr, now):

		self.pipe = pipe
		self.address = newAddr
		self.routingTable = [[] for i in range(addrBits)]
		self.bucketLastAccess = [now for i in range(addrBits)]
		self.maxBucketSize = 20
		self.maxBucketSizeHard = 50
		self.bucketRefreshTimeout = 60.0 #sec
		self.timeoutPing = 0.5
		self.timeoutFindNode = 0.5
		self.bucketOffsets = [0]
		self.queries = {}
		for v1v2 in zip(range(1, addrBits+1), range(-1, -addrBits-2, -1)):
			self.bucketOffsets.extend(v1v2)
		self.bootstrap = None
		self.connection = None
		self.sock = None
		self.refreshBucketActions = []
		self.shutDownRequested = False
		self.enableGoodbye = True

	def Bootstrap(self, joinConnection, RequestSocket):
		self.bootstrap = Bootstrap(self, joinConnection, RequestSocket)
	
	def Ready(self):
		if self.bootstrap is None: return False
		return not self.bootstrap.running

	def FindAddrInRoutingTable(self, addr, isBoot=False):
		assert addr is not None or isBoot
		assert addr is not self.address
		if addr == self.address: return None, None, None
		dist = Distance(addr, self.address)
		bucketNum = int(math.log(dist, 2))
		bucket = self.routingTable[bucketNum]
		for routingItem in bucket:
			if routingItem.addr == addr:
				return routingItem, bucket, bucketNum
		return None, bucket, bucketNum

	def AddAddrToRoutingTable(self, addr, connection, onlyMentioned, isBoot=False):
		assert addr is not None or isBoot
		assert self.address is not None
		routingItem, bucket, bucketNum = self.FindAddrInRoutingTable(addr, isBoot)
		assert bucket is not None
		newToMe = True
		if routingItem is not None:
			bucket.remove(routingItem)
			newToMe = False

		now = time.time()
		if routingItem is None:
			routingItem = RoutingItem(addr, connection)
		bucket.append(routingItem)
		self.bucketLastAccess[bucketNum] = now

		#This node is probably unaware of our existence, so give it a ping	
		if newToMe and onlyMentioned:
			self.PingAddr(addr)

	def RemoveAddrFromRoutingTable(self, addr, isBoot=False):
		routingItem, bucket, bucketNum = self.FindAddrInRoutingTable(addr, isBoot)

		if routingItem is not None:
			bucket.remove(routingItem)

	def DemoteAddrInRoutingTable(self, addr):
		routingItem, bucket, bucketNum = self.FindAddrInRoutingTable(addr)

		if routingItem is not None:
			bucket.remove(routingItem)
			routingItem.failures += 1
			bucket.insert(0, routingItem)

	def GenerateAddrInBucket(self, bucketNum):
		prefix = (self.address >> (bucketNum+1)) << (bucketNum+1) #Prefix bits remain unchanged
		diffBit = ((self.address >> (bucketNum)) ^ 0x1) << (bucketNum) #Diff bit must change
		if bucketNum > 0:
			randBits = random.getrandbits(bucketNum) #Random bits may change
		else:
			randBits = 0
		addr = prefix | diffBit | randBits

		#Check result
		bucketNum2 = int(math.log(Distance(addr, self.address), 2))
		assert bucketNum == bucketNum2

		return addr

	def FindRouteByNonce(self, nonce):
		for bucketNum, bucket in enumerate(self.routingTable):
			for routingItem in bucket:
				if routingItem.HasNonce(nonce):
					return routingItem, bucket, bucketNum
		return None, None, None

	def FindClosestInRouting(self, addr, numMatches, ignoreList=set()):
		#print (self.address, "FindClosestInRouting")
		assert addr is not None
		accelerated = True
		result = []

		if accelerated:

			#Accelerated search bucket at optimal distance
			#First find starting bucket
			if addr != self.address:
				dist = Distance(addr, self.address)
				bucketNum = int(math.log(dist, 2))
			else:
				bucketNum = -1
			
			#Search nearby buckets
			for basketOffset in self.bucketOffsets:

				basketAbsIndex = bucketNum+basketOffset
				if basketAbsIndex < 0 or basketAbsIndex >= len(self.routingTable):
					continue
				basket = self.routingTable[basketAbsIndex]
				bestDistAddr = []
				for routingItem in basket:
					if routingItem.failures > 0: continue
					if routingItem.addr in ignoreList: continue
					if routingItem.removePending: continue
					diff = Distance(addr, routingItem.addr)
					bestDistAddr.append((diff, routingItem))
				
				bestDistAddr.sort(key=lambda a: a[0])
				result.extend([(da[1].addr, da[1].connection) for da in bestDistAddr[:numMatches]])

				if len(result) >= numMatches:
					break

			if len(result) >= numMatches:
				result = result[:numMatches]
				assert len(result) == len(set(result)) #Must be unique
				return result

		else:

			#Exhaustive search ignoring failing nodes
			bestDistAddr = []
			for bucket in self.routingTable:
				for routingItem in bucket:
					if routingItem.addr in ignoreList: continue
					if routingItem.failures > 0: continue
					if routingItem.removePending: continue
					diff = Distance(addr, routingItem.addr)
					bestDistAddr.append((diff, routingItem))

			bestDistAddr.sort(key=lambda a: a[0])
			result = [(da[1].addr, da[1].connection) for da in bestDistAddr[:numMatches]]

			if len(result) >= numMatches: 
				assert len(result) == len(set(result)) #Must be unique
				return result
				
		#Exhaustive search of failing nodes (use as last resort)
		bestDistAddr = []
		for bucket in self.routingTable:
			for routingItem in bucket:
				if routingItem.addr in ignoreList: continue
				if routingItem.failures == 0: continue
				if routingItem.removePending: continue
				diff = Distance(addr, routingItem.addr)
				bestDistAddr.append((diff, routingItem))

		bestDistAddr.sort(key=lambda a: a[0])

		result = result + [(da[1].addr, da[1].connection) for da in bestDistAddr][:numMatches]
		assert len(result) == len(set(result)) #Must be unique
		return result

	def TimedUpdate(self):

		now = time.time()
		
		if self.sock is not None:
			self.sock.ReceiveMessage()

		if self.pipe is not None:
			while self.pipe.poll(0.0):
				obj = self.pipe.recv()
				if obj[0] == "find_node":
					self.FindClosestAsync(obj[1], listeners=[FindNodeResultToPipe(self.pipe)])
				if obj[0] == "shutdown":
					self.shutDownRequested = True

		if self.bootstrap:
			self.bootstrap.Update()

		#Ping any failed nodes in routing table
		for bucket in self.routingTable:
			for routingItem in bucket:
				if routingItem.failures == 0: continue
				if routingItem.removePending: continue
				if routingItem.addr is None: continue #Ignore the boot node
				self.PingAddr(routingItem.addr) #Removes item if failure

		#If we have too many bucket entries than the soft limit, check the oldest by ping
		for bucketNum, bucket in enumerate(self.routingTable):
			numOverLimit = len(bucket) - self.maxBucketSize
			if numOverLimit <= 0: continue

			#print (self.address, "has too many (",len(bucket),") entries in bucket", bucketNum)
			for routingItem in bucket[:numOverLimit]:
				if routingItem.removePending: continue
				if routingItem.addr is None: continue #Ignore the boot node
				self.PingAddr(routingItem.addr) #Removes item if failure

		#Check if we hit the hard limit for bucket size
		for bucketNum, bucket in enumerate(self.routingTable):
			numOverLimit = len(bucket) - self.maxBucketSizeHard
			if numOverLimit <= 0: continue
			removable = []
			for routingItem in bucket:
				if routingItem.activePing is None: continue
				if routingItem.removePending: continue
				if len(routingItem.findNode) > 0: continue
				if routingItem.addr is None: continue #Ignore the boot node
				removable.append(routingItem)

			#Inspired by Improving the Performance and Robustness of Kademlia-Based Overlay Networks
			#Andreas Binzenhöfer, Holger Schnabel
			#Find nodes that are nearest to own address and try to retain them.
			removableDistList = [(Distance(ri.addr, self.address), ri.failures, ri) for ri in removable]
			removableDistList.sort(key=lambda a:a[0]*(a[1]+1), reverse=True)
			
			for dist, failures, routingItem in removableDistList[:numOverLimit]:
				routingItem.removePending = True #Flag for removal
				
		#Update buckets that have not been used in a while
		for bucketNum, bucket in enumerate(self.routingTable):
			mostRecent = self.bucketLastAccess[bucketNum]
			elapse = now - mostRecent
			
			if elapse > self.bucketRefreshTimeout:
				refreshBucket = RefreshBucket(self, bucketNum)
				refreshBucket.Refresh()
				self.refreshBucketActions.append(refreshBucket)

				self.bucketLastAccess[bucketNum] = now

		deleteNow = []
		for rba in self.refreshBucketActions:
			if rba.deletePending is False: continue
			deleteNow.append(rba)
		for rba in deleteNow:
			deleteNow.remove(rba)

		#Check for ping time outs
		for bucket in self.routingTable:
			for routingItem in bucket:
				if routingItem.activePing is None: continue

				elapse = now - routingItem.activePing[0]
				if elapse > self.timeoutPing:
					#print (self.address, "Ping timed out", routingItem.addr)

					#Ping is taking too long
					self.AsyncPingAddrFailed(routingItem)
					routingItem.activePing = None

		expired = []
		for addr, fc in self.queries.items():
			fc.Update()

			if fc.statusFinished:
				expired.append(addr)
		for addr in expired:
			del self.queries[addr]

		for bucket in self.routingTable:
			for routingItem in bucket:
				
				#Check for find node time outs
				expired = []
				for addr, findQ in routingItem.findNode.items():

					elapse = now - findQ[0]
					if elapse > self.timeoutFindNode:
						#print (self.address, "Find node timed out", addr)
						listeners = findQ[1]
						for li in listeners:
							li.AsyncFindNodeFailed(routingItem.addr)
						expired.append(addr)

				for exp in expired:
					del routingItem.findNode[exp]
		
		#Remove dead routing items
		for bucket in self.routingTable:
			for routingItem in bucket:
				if not routingItem.removePending: continue
				if routingItem.activePing is not None: continue
				if len(routingItem.findNode) > 0: continue
				if routingItem.addr is None: continue #Ignore the boot node

				self.RemoveAddrFromRoutingTable(routingItem.addr)

	def Shutdown(self):
		if self.enableGoodbye:
			for bucket in self.routingTable:
				for routingItem in bucket:
					self.sock.SendMessage(
						routingItem.addr, routingItem.connection, self.address, self.connection, 
							msgpack.packb({'type': 'goodbye', 'sender': self.address}))

	##### Start a client request #####

	def PingAddr(self, addr, inBoot=False):
		#print (self.address, "pings", addr)
		routingItem, bucket, bucketNum = self.FindAddrInRoutingTable(addr, inBoot)

		if routingItem is None and not inBoot:
			raise RuntimeError("Ping addr not in routing table")
		if routingItem.activePing is None:
			nonce = random.randint(0, pow(2, 64))
			routingItem.activePing = [time.time(), nonce]
			ret = self.sock.SendMessage(
				routingItem.addr, routingItem.connection, self.address, 
					self.connection, msgpack.packb({'type': 'ping', 'nonce': nonce, 'sender': self.address}))

	def FindClosestAsync(self, addr, listeners=[], isBoot=False):
		assert addr is not None

		if addr in self.queries:
			fc = self.queries[addr]
			if fc.statusFinished:
				del self.queries[addr]

		if addr not in self.queries:

			fc = FindClosest(self, addr, isBoot)
			self.queries[addr] = fc
			fc.listeners = listeners.copy()

		else:
			fc = self.queries[addr]
			fc.listeners.extend(listeners)

	def SendDownlistNotify(self, addr, failedAddrs):
		routingItem, bucket, bucketNum = self.FindAddrInRoutingTable(addr)

		if routingItem is not None:
			self.sock.SendMessage(
				routingItem.addr, routingItem.connection, self.address, self.connection, 
					msgpack.packb({'type': 'downlist', 'sender': self.address, 'failed': failedAddrs}))		

	##### Handle async client events #####

	def AsyncPingAddrSuccess(self, routingItem):
		#print ("AsyncPingAddrSuccess")
		self.AddAddrToRoutingTable(routingItem.addr, routingItem.connection, False)

	def AsyncPingAddrFailed(self, routingItem):
		#print ("AsyncPingAddrFailed")
		routingItem.removePending = True

	##### Server side #####

	def HandleFindClosestRequests(self, addr, requestAddr, requestConnection, nonce):
		results = self.FindClosestInRouting(addr, addrBits)

		#Never return our boot node
		results = [(addr, connection) for addr, connection in results if addr is not None]
		
		self.sock.SendMessage(
			requestAddr, requestConnection, self.address, self.connection, msgpack.packb(
				{'type': 'find_node_response', 'result': results, 'nonce': nonce, 
					'sender': self.address, 'responding_to': requestAddr}))

		self.AddAddrToRoutingTable(requestAddr, requestConnection, False)

	def HandlePing(self, requestAddr, requestConnection, nonce):
		self.AddAddrToRoutingTable(requestAddr, requestConnection, False)

		self.sock.SendMessage(
			requestAddr, requestConnection, self.address, self.connection, msgpack.packb(
				{'type': 'ping_response', 'nonce': nonce, 
					'sender': self.address, 'responding_to': requestAddr}))

	def HandleGoodbye(self, fromAddr):
		self.DemoteAddrInRoutingTable(fromAddr)

	def HandleDownlist(self, failed):
		for addr in failed:
			self.DemoteAddrInRoutingTable(addr)

	def ReceiveMessage(self, fromConnection, msg):

		assert fromConnection != self.connection
		now = time.time()
		msgDecoded = msgpack.unpackb(msg)
		msgType = msgDecoded['type']
		fromAddr = msgDecoded['sender']
		assert fromAddr != self.address
		inBoot = not self.Ready()

		#print (msgType, fromAddr, "->", self.address)
		if msgType == 'find_node':
			self.HandleFindClosestRequests(msgDecoded['address'], fromAddr, fromConnection, msgDecoded['nonce'])
			return
		elif msgType == 'ping':
			ret = self.HandlePing(fromAddr, fromConnection, msgDecoded['nonce'])
			return msgpack.packb(ret)
		elif msgType == 'goodbye':
			ret = self.HandleGoodbye(fromAddr)
			return
		elif msgType == 'ping_response':
			routingItem, bucket, bucketNum = self.FindAddrInRoutingTable(fromAddr)
			msgNonce = msgDecoded['nonce']
			if routingItem is None:
				print ("b", inBoot)
			if routingItem is None and inBoot:
				#Ensure boot node messages are routed correctly
				routingItem, bucket, bucketNum = self.FindRouteByNonce(msgNonce)
				print ("c", routingItem)
		
			if routingItem is not None:
				ok = routingItem.HandlePingResponse(msgNonce)
				if ok:
					self.AsyncPingAddrSuccess(routingItem)
			else:
				print (self.address, "Failed to route", msgType, fromAddr, fromConnection)

		elif msgType == 'find_node_response':
			routingItem, bucket, bucketNum = self.FindAddrInRoutingTable(fromAddr)
			msgNonce = msgDecoded['nonce']
			if routingItem is None and inBoot:
				#Ensure boot node messages are routed correctly
				routingItem, bucket, bucketNum = self.FindRouteByNonce(msgNonce)

			if routingItem is not None:
				routingItem.HandleFindNodeSuccess(msgNonce, fromAddr, fromConnection, msgDecoded['result'])
			else:
				print (self.address, "Failed to route", msgType, fromAddr, fromConnection, self.routingTable)

		elif msgType == 'downlist':
			failed = msgDecoded['failed']
			self.HandleDownlist(failed)
			
	##### Debug #####

	def DumpToFile(self, fi):
		for bucketNum, bi in enumerate(self.routingTable):
			fi.write("{},{},{}\n".format(self.address, bucketNum, bi))

def DumpState(t, diagnostic):
	#Dump diagnostic info
	fi = open("dump.txt", "wt")
	fi.write("seed {}\n".format(t))
	#nodeLi = list(network.nodes)
	#nodeLi.sort()
	#for n in nodeLi:
	#	network.nodes[n].DumpToFile(fi)
	if diagnostic is not None:
		for li in diagnostic:
			fi.write("{}\n".format(li))
	fi.close()

def NodeProcess(q, newAddr, joinConnection, RequestSocket):

	n = Node(q, newAddr, time.time())
	n.Bootstrap(joinConnection, RequestSocket)
	while not n.shutDownRequested:

		n.TimedUpdate()
		#time.sleep(0.0)

class TestManager(object):
	def __init__(self):
		self.nodePipes = {}
		self.nodeProcesses = {}
		self.nodeConnection = {}
		self.nodeReady = {}
		self.nodesByPipe = {}

		self.findNodeResults = []
		self.findNodePending = []

	def UpdatePipes(self):

		hasData = mpc.wait(self.nodePipes.values(), timeout=0.05)
		for pipe in hasData:
			addr = self.nodesByPipe[pipe]
			pipeReady = True
			while pipeReady:
				obj = pipe.recv()
				if obj[0] == "address":
					self.nodeConnection[addr] = obj[2]
				elif obj[0] == "ready":
					self.nodeReady[addr] = obj[1]
				elif obj[0] == "find_node_response":
					self.findNodeResults.append((time.time(), obj[1:]))
				pipeReady = pipe.poll(0.0)

	def RequestAddress(self):
		count = 0
		while True:
			newAddr = random.getrandbits(addrBits)
			if newAddr not in self.nodeProcesses: 
				break
			if count > 10000:
				return None
			count += 1
		return newAddr

	def Run(self):

		while True:

			action = random.randint(0, 2)

			if action == 0:
				#Add some nodes
				maxNodes = random.randint(0, 100)
				if len(self.nodeProcesses) < maxNodes:
					print ("Increase nodes to", maxNodes)

				while len(self.nodeProcesses) < maxNodes:
					if len(self.nodeProcesses) > 0:
						#Only join a node that is actually ready
						readyNodes = [n for n, r in self.nodeReady.items() if r]
						print ("ready", len(readyNodes), "created", len(self.nodeProcesses))
						while len(readyNodes) == 0:
							#Wait for first node to become visible on the network
							self.UpdatePipes()

							time.sleep(0.0)

							readyNodes = [n for n, r in self.nodeReady.items() if r]

						joinNode = random.choice(readyNodes)
						joinAddr = joinNode
						joinConnection = self.nodeConnection[joinNode]
					else:
						joinAddr = None
						joinConnection = None

					newAddr = self.RequestAddress()

					pipe1, pipe2 = multiprocessing.Pipe()
					p = multiprocessing.Process(target=NodeProcess, 
						args=(pipe1, newAddr, joinConnection, connection.RequestSocketUdp))
					self.nodePipes[newAddr] = pipe2
					self.nodeProcesses[newAddr] = p
					self.nodeReady[newAddr] = False
					self.nodesByPipe[pipe2] = newAddr
					self.nodeConnection[newAddr] = None
					p.start()

			elif action == 1:

				readyNodes = [n for n, r in self.nodeReady.items() if r]
				if len(readyNodes) > 90:

					#Remove some nodes
					print ("Try to remove some nodes")
					nodesToRemove = random.randint(0, 5)
					removeCount = 0
					for j in range(nodesToRemove):
						if len(self.nodeProcesses) == 0: break

						addr = random.choice(list(self.nodeProcesses))
						nodePipe = self.nodePipes[addr]
						nodePipe.send(("shutdown",))
						self.nodeProcesses[addr].join()

						del self.nodePipes[addr]
						del self.nodeProcesses[addr]
						del self.nodeConnection[addr]
						del self.nodeReady[addr]
						del self.nodesByPipe[nodePipe]

						removeCount += 1
					if removeCount > 0:
						print ("Remove", removeCount, "nodes")

			elif action == 2 and len(self.nodeProcesses) >= 2:

				#Check routeability
				readyNodes = [n for n, r in self.nodeReady.items() if r]
				if len(readyNodes) > 2:

					self.findNodeResults = []
					self.findNodePending = []
					
					print ("checking routing")
					for i in range(random.randint(1, 10)):
						addr1 = random.choice(readyNodes)
						addr2 = random.choice(readyNodes)
						if addr1 == addr2: continue
						peer1Pipe = self.nodePipes[addr1]
						peer1Pipe.send(("find_node", addr2))
						self.findNodePending.append(addr2)

					startTime = time.time()

					#Wait for response
					while len(self.findNodeResults) < len(self.findNodePending):
						self.UpdatePipes()
						time.sleep(0.0)
		
					resultsChk = []
					diagnostics = []
					for arrivalTime, (targetAddr, bestAddr, bestConnection, queryDgnstc) in self.findNodeResults:

						duration = arrivalTime - startTime
						ok = targetAddr == bestAddr
						resultsChk.append(ok)
						diagnostics.append(queryDgnstc)

						if duration > 3.0:
							for li in queryDgnstc:
								print (li)
							exit(0)

						if not ok:
							DumpState(t, queryDgnstc)
							print ("Routing error detected", addr1, targetAddr, bestAddr)
							exit(0)

					if len(resultsChk) > 0:
						routability = sum(resultsChk) / len(resultsChk)
						print ("routability test", routability, len(resultsChk))
						if len(resultsChk) > 0 and routability < 1.0: 
							exit(0)

			self.UpdatePipes()
			time.sleep(0.0)
			#readyNodes = [n for n, r in self.nodeReady.items() if r]
			#print ("ready", len(readyNodes), "created", len(self.nodeProcesses))


if __name__=="__main__":

	t = int(time.time())
	print (t)
	random.seed(t)

	testManager = TestManager()
	testManager.Run()

