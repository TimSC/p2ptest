import p2p
import connection
import random
import time
import multiprocessing

if __name__=="__main__":
	
	newAddr = random.getrandbits(16)

	joinAddr = 21170
	joinConnection = "0.0.0.0:48060"

	pipe1, pipe2 = multiprocessing.Pipe()

	n = p2p.Node(pipe1, newAddr, time.time())
	n.Bootstrap(joinAddr, joinConnection, connection.RequestSocketUdp)
	while not n.shutDownRequested:

		n.TimedUpdate()

		while pipe2.poll(0.0):
			print (pipe2.recv())


